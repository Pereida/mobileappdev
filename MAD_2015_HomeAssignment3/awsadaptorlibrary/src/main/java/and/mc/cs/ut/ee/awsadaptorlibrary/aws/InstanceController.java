package and.mc.cs.ut.ee.awsadaptorlibrary.aws;

import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;

import java.io.InputStream;
import java.util.List;

import and.mc.cs.ut.ee.awsadaptorlibrary.AwsInterface;
import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceMediator;


public class InstanceController {

    private static final String TAG = InstanceController.class.getName();

    String endPoint;
	AWSCredentials credentials;
	AmazonEC2Client ec2Client;
    Instance instance;

    private CloudServiceMediator mediator;

    /**
     *
     * @param cloudServiceMediator
     * @param credentialsIS AWS credentials file (e.g. AwsCredentials.properties) which contains
     *                      the secretKey and accessKey values
     * @throws Exception
     */
    public InstanceController(CloudServiceMediator cloudServiceMediator, InputStream credentialsIS) throws Exception {
        this.mediator = cloudServiceMediator;
        credentials = new PropertiesCredentials(credentialsIS);

        if (credentials.getAWSAccessKeyId().isEmpty() ||
                credentials.getAWSSecretKey().isEmpty()){
            throw new Exception(" ERROR: EC2 Client setup failed. " +
                    "You have not specified your AWS credentials in the" +
                    " \'AwsCredentials.properties\' file");
        }
        else {
            ec2Client = new AmazonEC2Client(credentials);
        }
    }

    public void terminateInstance() {
        if (instance != null){
            TerminateInstanceTask instanceTask = new TerminateInstanceTask();
            instanceTask.execute(this);
        }
    }
    /** Sets the current instance to be the one which corresopnds to the
     * provided instance id. If the provided instance id is an instance which
     * can be conencted to, the current instance is set to be that instance.
     * Otherwise,
     * @param instanceId
     * @param activity
     */
    public void connectToInstance(String instanceId, AwsInterface activity) {
        if (instance == null){
            ConnectToInstanceTask instanceTask = new ConnectToInstanceTask(this,activity);
            instanceTask.execute(instanceId);
        }
    }

    public Instance connectToInstance(String instanceId) throws Exception {
        DescribeInstancesRequest describeRequest =
                new DescribeInstancesRequest().withInstanceIds(instanceId);

        DescribeInstancesResult describeResult = ec2Client.describeInstances(describeRequest);
        Log.i(TAG, describeResult.toString());

        List<Reservation> reservations = describeResult.getReservations();
        if (!reservations.isEmpty()){
            List<Instance> instances= reservations.get(0).getInstances();
            if (!instances.isEmpty()) {
                this.instance = instances.get(0);
                return instance;
            }
        }
        throw new Exception("Couldn't retrieve instance");
    }

    /** Starts an AsyncTask which launches an instance according
     * to the provided configuration
     * @param conf
     */
    public void launchInstance(LaunchConfiguration conf) {
        InstanceLauncher launcher = new InstanceLauncher(this,conf);
        if (instance == null){
            RunInstanceTask instanceTask = new RunInstanceTask(mediator);
            instanceTask.execute(launcher);
        } else {
            Log.i(TAG, "Did not launch new instance because current instance was not null.");
        }
    }


    public AmazonEC2Client getEc2Client() {
        return ec2Client;
    }
    public Instance getInstance() {
        return instance;
    }
    public void setInstance(Instance instance) {
        this.instance = instance;
    }
    public void setEndPoint(String endPoint) {this.endPoint = endPoint; }
}
