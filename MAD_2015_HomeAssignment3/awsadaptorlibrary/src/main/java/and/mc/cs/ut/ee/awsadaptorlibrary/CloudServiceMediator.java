package and.mc.cs.ut.ee.awsadaptorlibrary;

import android.util.Log;

import com.amazonaws.services.ec2.model.Instance;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import and.mc.cs.ut.ee.awsadaptorlibrary.aws.InstanceController;
import and.mc.cs.ut.ee.awsadaptorlibrary.aws.LaunchConfiguration;
import and.mc.cs.ut.ee.awsadaptorlibrary.aws.AwsInstanceInterface;

/**
 * Created by jaks on 20/02/15.
 */
public class CloudServiceMediator implements AwsInstanceInterface {

    public interface InstanceListener{
        void instanceLaunchComplete();
        void instanceLaunchInProgress();
    }

    public static final String TAG = CloudServiceMediator.class.getName();

    public static final int INSTANCE_RUNNING = 1;
    public static final int INSTANCE_LAUNCHING = 2;
    public static final int INSTANCE_TERMINATING = -1;
    public static final int INSTANCE_TERMINATED = -2;
    public static final int INSTANCE_UNKNOWN = 0;

    int state = INSTANCE_UNKNOWN;

    InstanceController instanceController;
    private List<InstanceListener> listeners;

    private InputStream awsCredentialsInputStream;


    public CloudServiceMediator(InputStream awsCredentialsIS) {
//        this.uiInterface = uiInterface; TODO decide if this were better than using listeners
        this.listeners = new ArrayList<>();
        this.awsCredentialsInputStream = awsCredentialsIS;
    }

    public void launchInstance(LaunchConfiguration launchConf){
        try {
            instanceController = createInstanceControllerIfDoesntExist();
            instanceController.launchInstance(launchConf);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private InstanceController createInstanceControllerIfDoesntExist() {
        if (instanceController == null) {
            try {
                InstanceController controller = new InstanceController(this, awsCredentialsInputStream);
                controller.setEndPoint("ec2.us-east-1.amazonaws.com"); //TODO remove hardcode
                return controller;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instanceController;
    }

    @Override
    public void onInstanceUpdate(Instance i, int stateCode) {
        if (instanceController != null) instanceController.setInstance(i);
        if (stateCode == Utils.INSTANCE_RUNNING) {
            //Notify listeners
            for (InstanceListener listener : listeners){
                listener.instanceLaunchComplete();
            }
        } else if (stateCode == Utils.INSTANCE_LAUNCHING){
            for (InstanceListener listener : listeners){
                listener.instanceLaunchInProgress();
            }
        }
    }


    /** Convenience method for retrieving the instance, given it has been launched.
     * Use this for example when you have a launched an instance, closed the app, to re-gain
     * a reference to the instance.
     *
     * @return null if no instance running
     */
    public Instance getInstance(){
        Instance instance = null;
        if (instanceController != null){
            instance = instanceController.getInstance();
            if (instance != null) return instance;
        }
        throw new Error("Instance not available, instance = null. Have you launched an instance?");
    }

    public void addListener(InstanceListener toAdd) {
        listeners.add(toAdd);
    }
}

