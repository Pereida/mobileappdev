package and.mc.cs.ut.ee.awsadaptorlibrary.scp;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by Jakob on 14.11.2014.
 *
 * This is the main interface for transmitting files and commands via SCP.
 */
public class ScpManager {

    public final String TAG = ScpManager.class.getSimpleName();
    ExtendedJSch jsch;

//    String keyFileName;

    String username;
    String host;
    int port;

    Session session;
    private String password;

    public ScpManager() {
        this.jsch = new ExtendedJSch();
        JSch.setLogger(new AndroidScpLogger());
    }

    static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        // 1 for error,
        // 2 for fatal error,
        // -1
        if (b == 0) return b;
        if (b == -1) return b;

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            }
            while (c != '\n');
            if (b == 1) { // error
                System.out.print(sb.toString());
            }
            if (b == 2) { // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }

    /**
     * Sets all the parameters required to establish a SCP connection.
     *
     * @param username
     * @param host
     * @param port
     * @param manager This is used to look for the keyFile
     * @param keyFile This is the ssh key file (e.g. .pem) used to connect via SSH/SCP
     */
    public void configureSession(String username, String host, int port, AssetManager manager, String keyFile) {
        this.username = username;
        this.host = host;
        this.port = port;
//        this.keyFileName = keyFile;
        try {
            jsch.setIdentityKey(manager, keyFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets all the parameters required to establish a SCP connection.
     *
     * @param username
     * @param host
     * @param port
     * @param keyFileIS This is the ssh key file (e.g. .pem) used to connect via SSH/SCP
     */
    public void configureSession(String username, String host, int port, InputStream keyFileIS) {
        this.username = username;
        this.host = host;
        this.port = port;
//        this.keyFileName = keyFile;
        try {
            jsch.setIdentityKey(keyFileIS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    public void configureSession(String username, String password, String host, int port) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;

    }

    public void initFileSendTransfer(final InputStream fileInStream, String filename, final Long size) {
        final String localFile = filename;
        final String remoteFile = localFile;

        try {
            openToChannelAndTransfer(remoteFile, localFile, fileInStream, size);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void createSession() {
        Log.i(TAG, "createSession() ");
        int retriesLeft = 45;
        try {
            session = jsch.getSession(username, host, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }

        if (password != null){
            session.setPassword(password);
            // Avoid asking for key confirmation
            Properties prop = new Properties();
            prop.put("StrictHostKeyChecking", "no");
            session.setConfig(prop);
        }


        while (retriesLeft > 0 && !session.isConnected()) {
            retriesLeft--;
            try {
                session.connect(2000);
            } catch (JSchException e) {
                Log.e(TAG, e.toString());
            }
        }
        if (session.isConnected()) Log.i(TAG, "***session connected");
    }

    public void killSession() {
        session.disconnect();
        session = null;
    }

    private void openToChannelAndTransfer(String remoteFile, String localFile, InputStream fileInStream, Long size) throws JSchException, IOException {
        // exec 'scp -t rfile' remotely
        String command = "scp -t " + remoteFile;
        Channel channel = openChannelWithCommand(command);
        channel.connect();

        // get I/O streams for remote scp
        OutputStream remoteOut = channel.getOutputStream();
        InputStream remoteIn = channel.getInputStream();
        failIfNotAcknowledged(remoteIn);

        // send "C0644 filesize filename", where filename should not include '/'

        command = "C0644 " + size + " ";
        if (localFile.lastIndexOf('/') > 0) {
            localFile = localFile.substring(localFile.lastIndexOf('/') + 1);
        }
        command += localFile + "\n";

        remoteOut.write(command.getBytes());
        remoteOut.flush();
        failIfNotAcknowledged(remoteIn);

        // send content of file
        byte[] buf = new byte[1024];
        writeFileToRemoteOut(fileInStream, remoteOut, buf);

        // send '\0'
        buf[0] = 0; remoteOut.write(buf, 0, 1); remoteOut.flush();
//        remoteOut.write(0); //!TODO CHECK IF THIS ALSO WORKS

        failIfNotAcknowledged(remoteIn);

        channel.disconnect();
        remoteOut.close();
        remoteIn.close();
    }

    private Channel openChannelWithCommand(String command) throws JSchException {
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);
        return channel;
    }

    private void writeFileToRemoteOut(InputStream fileInStream, OutputStream remoteOut, byte[] buf) throws IOException {
        while (true) {
            int len = fileInStream.read(buf, 0, buf.length);
            if (len <= 0) break;
            remoteOut.write(buf, 0, len);
            remoteOut.flush();
        }
        fileInStream.close();
//        fileInStream = null;
    }

    private void failIfNotAcknowledged(InputStream remoteIn) throws IOException {
        if (checkAck(remoteIn) != 0) {
            System.exit(0);
        }
    }

    /**
     * Executes the given command on the remote machine and logs the response
     */
    public void sendCommandAndLogResponse(String command) {
        Log.v(TAG, "sendCommand()");
        try {
            createSession();

            Channel channel = openChannelWithCommand(command);
            channel.connect();

            readAndLogResponse((ChannelExec) channel);
            Log.v(TAG, "Disconnecting & killing session in sendCommand()");
            channel.disconnect();
            killSession();

        } catch (JSchException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Executes the given command on the remote machine and logs the response
     */
    public String sendCommand(String command) {
        Log.v(TAG, "sendCommand()");
        String result = "";
        try {
            createSession();

            Channel channel = openChannelWithCommand(command);
            channel.connect();

            result = readAndReturnResponse((ChannelExec) channel);
            Log.v(TAG, "Disconnecting & killing session in sendCommand()");
            channel.disconnect();
            killSession();



        } catch (JSchException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void readAndLogResponse(final ChannelExec channel) throws IOException, InterruptedException {
        InputStream input = channel.getInputStream();

        //start reading the input from the executed commands on the shell
        byte[] tmp = new byte[1024];
        while (true) {
            while (input.available() > 0) {
                int i = input.read(tmp, 0, 1024);
                if (i < 0) break;
                Log.i(TAG, new String(tmp, 0, i));
            }
            if (channel.isClosed()) {
                Log.i(TAG, "channel Closed with exit code=" + channel.getExitStatus());
                break;
            }
            Thread.sleep(1000);
        }
    }

    private String readAndReturnResponse(final ChannelExec channel) throws IOException, InterruptedException {
        InputStream input = channel.getInputStream();
        StringBuilder sb = new StringBuilder();

        //start reading the input from the executed commands on the shell
        byte[] tmp = new byte[1024];
        while (true) {
            while (input.available() > 0) {
                int i = input.read(tmp, 0, 1024);
                if (i < 0) break;
                sb.append(new String(tmp, 0, i));
                Log.i(TAG, new String(tmp, 0, i));

                return sb.toString();
            }
            if (channel.isClosed()) {
                Log.i(TAG, "channel Closed with exit code=" + channel.getExitStatus());
                break;
            }
            Thread.sleep(1000);
        }
        return "";
    }

    /**
     * Gets the given files InputStream and length,
     * calls the initFileSendTransfer function with these values.
     *
     * <p>This method uses inputStream.available() to determine file length.
     * this might be unreliable, see: http://developer.android.com/reference/java/io/InputStream.html#available%28%29</p>
     *
     * @param context - this context's resources are used to search for the given raw resource
     * @param resourceId - the resource to send.
     *
     */
    public void sendFileFromRawResources(Context context, int resourceId) throws IOException {
        InputStream inputStream = context.getResources().openRawResource(resourceId);
        String resourceName = context.getResources().getResourceEntryName(resourceId);
        //Danger, available() might be unreliable, see http://developer.android.com/reference/java/io/InputStream.html#available%28%29
        long length = inputStream.available();

        sendInputStreamAsFile(inputStream, resourceName, length);
    }

    /**
     * Gets the given files InputStream and length,
     * calls the initFileSendTransfer function with these values
     *
     * @param assets
     * @param assetName
     */
    public void sendFileFromAssets(AssetManager assets, String assetName) throws IOException {
        long length = assets.openFd(assetName).getLength();
        InputStream inputStream = assets.open(assetName);

        sendInputStreamAsFile(inputStream, assetName, length);
    }

    public void sendInputStreamAsFile(InputStream inputStream, String fileName, long length) throws IOException {
        createSession();
        initFileSendTransfer(inputStream, fileName, length);
        killSession();
    }

    public void sendInputStreamAsFile(InputStream inputStream, String fileName) throws IOException {
        sendInputStreamAsFile(inputStream, fileName, inputStream.available());
    }

    public void receiveFile(String fileName, File path) {
        createSession();
        initFileReceiveTransfer(fileName, path);
        killSession();
    }


    /** Assumes session is connected*/
    public void initFileReceiveTransfer(String outputFilename, File path) {
        FileOutputStream fos=null;
        try{
            String rfile= outputFilename;
            String lfile= outputFilename;

            File mFile = new File(path, outputFilename);
            fos = new FileOutputStream(mFile);

//            String prefix=null;
//            if(new File(lfile).isDirectory()){
//                prefix=lfile+File.separator;
//            }

            // exec 'scp -f rfile' remotely
            String command="scp -f "+rfile;
            Channel channel=session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command);

            // get I/O streams for remote scp
            OutputStream remoteOut=channel.getOutputStream();
            InputStream remoteIn=channel.getInputStream();

            channel.connect();

            byte[] buf=new byte[1024];

            // send '\0'
            buf[0]=0; remoteOut.write(buf, 0, 1); remoteOut.flush();

            while(true){
                int c=checkAck(remoteIn);
                if(c!='C'){
                    break;
                }

                // read '0644 '
                remoteIn.read(buf, 0, 5);

                long filesize=0L;
                while(true){
                    if(remoteIn.read(buf, 0, 1)<0){
                        // error
                        break;
                    }
                    if(buf[0]==' ')break;
                    filesize=filesize*10L+(long)(buf[0]-'0');
                }

                String file=null;
                for(int i=0;;i++){
                    remoteIn.read(buf, i, 1);
                    if(buf[i]==(byte)0x0a){
                        file=new String(buf, 0, i);
                        break;
                    }
                }
                //System.out.println("filesize="+filesize+", file="+file);

                // send '\0'
                buf[0]=0; remoteOut.write(buf, 0, 1); remoteOut.flush();

                // read a content of lfile
//                fos=new FileOutputStream(prefix==null ? lfile : prefix+file);
                int foo;
                while(true){
                    if(buf.length<filesize) foo=buf.length;
                    else foo=(int)filesize;
                    foo=remoteIn.read(buf, 0, foo);
                    if(foo<0){
                        // error
                        break;
                    }
                    fos.write(buf, 0, foo);
                    filesize-=foo;
                    if(filesize==0L) break;
                }
                fos.close();
                fos=null;

                // send '\0'
                buf[0]=0; remoteOut.write(buf, 0, 1); remoteOut.flush();

                failIfNotAcknowledged(remoteIn);

                channel.disconnect();
                remoteOut.close();
                remoteIn.close();
            }
        }
        catch(Exception e){
            System.out.println(e);
            try{if(fos!=null)fos.close();}catch(Exception ee){}
        }
    }

}
