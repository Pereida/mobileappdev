# README #
**In order to use the AWS library, you must add AwsCredentials.properties and a keyfile to your assets folder!!!**

### AWS convenience library + demos ###

An Android Studio project consisting of 3 modules:

* A convenience library for launching and managing instances using Amazon AWS/EC2

* A "lecturer" demo app which launches an instance and provides an interactive command line to the launched instance in the UI

* A "student" demo app which uses SSH/SCP to connect to a remote SSH server (EC2 instance), transmit a photo there, process the photo, and receive the resulting processed photo.




### Who do I talk to? ###

* Code author jaks@ut.ee