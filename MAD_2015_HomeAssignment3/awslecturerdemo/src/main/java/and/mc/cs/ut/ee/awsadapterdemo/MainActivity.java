package and.mc.cs.ut.ee.awsadapterdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import and.mc.cs.ut.ee.awsadaptorlibrary.AwsInterface;
import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceCommunicator;
import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceMediator;
import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceMediator.InstanceListener;
import and.mc.cs.ut.ee.awsadaptorlibrary.aws.LaunchConfiguration;

public class MainActivity extends AppCompatActivity implements AwsInterface {

    private static final String TAG = MainActivity.class.getSimpleName();

    private EditText terminalEditText;
    private TextView consoleTextView;
    private ScrollView scrollView;
    private CloudServiceMediator cloudLauncher;
    CloudServiceCommunicator cloudCommunicator;

    private LaunchConfiguration cloudConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        terminalEditText = (EditText) findViewById(R.id.edittext_terminal);
        consoleTextView = (TextView) findViewById(R.id.textview_console);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        /** Initialize our Amazon AWS mediator */
        try {
            InputStream credentialsInputStream =
                    getApplicationContext().getAssets().open("AwsCredentials.properties");
            this.cloudLauncher = new CloudServiceMediator(credentialsInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startInstance(View v) {
        displayMessage("Started instance launch.");
        cloudConfig = new LaunchConfiguration(
                "m3.large",    //instance type
                "ami-098ed26c", //AMI id
                "jakob.mass",   //key name
                "sg-fd262b98"   //security group
        );

        cloudLauncher.addListener(new InstanceListener() {
            @Override
            public void instanceLaunchComplete() {
                //Do something with the launched instance
                consoleTextView.setText(""); //clear the console.
                displayMessage("Launched instance " + cloudLauncher.getInstance().getPublicIpAddress());
            }

            @Override
            public void instanceLaunchInProgress() {
                displayMessage("Waiting for launch to finish...");
            }
        });

        cloudLauncher.launchInstance(cloudConfig);
    }

    public void interactWithRunningInstance(View v) throws IOException {
        InputStream sshKeyFile = getAssets().open("jakobmass.pem");

        cloudCommunicator = new CloudServiceCommunicator(
                cloudLauncher.getInstance().getPublicIpAddress(),
                "ubuntu",
                22,
                sshKeyFile
        );

        cloudCommunicator.addListener(new CloudServiceCommunicator.SSHListener() {
            @Override
            public void commandResultReceived(String result) {
                displayMessage(result);
            }

            @Override
            public void fileSent() {}

        });

        String command = "echo --- CONNECTED TO: $(hostname)---;";
        displayMessage("Starting SSH... ");
        cloudCommunicator.doSendCommandTask(command);

    }

    public void sendCommand(View v) {
        String command = terminalEditText.getText().toString();
        terminalEditText.setText(null);
        displayMessage(command);
        cloudCommunicator.doSendCommandTask(command);
    }

    @Override
    public void showError(String msg) {
        consoleTextView.setText(msg);
    }

    @Override
    public void displayMessage(String msg) {
        consoleTextView.append("\n" +msg);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }





}
