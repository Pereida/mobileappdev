package and.mc.cs.ut.ee.awsstudentdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceCommunicator;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MainActivity extends AppCompatActivity {
    private static final int SELECT_PHOTO = 100;
    private static final int UNCOVER_PHOTO = 200;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static String DEBUG_FILE_TAG = "Writing";


    private CloudServiceCommunicator cloudCommunicator;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    // Added this part to give access to the MainActivity from Broadcast Receiver
    // ---------------------
    private TextView t1;
    private EditText etRegId;
    private static MainActivity mInst;

    public static MainActivity instance() {
        return mInst;
    }

    @Override
    public void onStart() {
        super.onStart();
        mInst = this;
    }

    @Override
    public void onStop() {
        super.onStop();
        mInst = null;
    }
    // ---------------------


    /**
     * Substitute your own project number here. This project number comes
     * from the Google Developers Console.
     */
    static final String PROJECT_NUMBER = "1021960077994";

    private GoogleCloudMessaging mGcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t1 = (TextView) findViewById(R.id.tv_messages);

        // Check whether Google Play Services are available
        if (checkPlayServices()) {
            mGcm = GoogleCloudMessaging.getInstance(this);
            String regId = getRegistrationId(this);
            Log.i(TAG, "Registration ID " + regId);
            if (PROJECT_NUMBER.equals("Your Project Number")) {
                new AlertDialog.Builder(this)
                        .setTitle("Needs Project Number")
                        .setMessage("GCM will not function until you set the Project Number to the one from the Google Developers Console.")
                        .setPositiveButton(android.R.string.ok, null)
                        .create().show();
            } else if (regId.isEmpty()) {
                registerInBackground(this);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK. Weather alerts will be disabled.");
            // Store regID as null
            storeRegistrationId(this, null);
        }
    }

    public void doWorkOnCloud(View v) throws IOException {
        cloudCommunicator = new CloudServiceCommunicator(
                "54.173.53.173", //host ip address
                "B50783", //username (your student code)
                "B50783", //password (your student code)
                22
        );

        cloudCommunicator.addListener(new CloudServiceCommunicator.SSHListener() {
            @Override
            public void commandResultReceived(String result) {
                Log.i(TAG, result);
            }

            @Override
            public void fileSent() {
                Log.i(TAG, "File sent");
            }
        });

        // Send a simple ECHO command just to see that we really are connected.
        String command = "echo --- CONNECTED TO: $(hostname)---;";
        cloudCommunicator.doSendCommandTask(command);

        //// Intent to choose photo
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }

    public void unCover(View v) throws IOException {
        cloudCommunicator = new CloudServiceCommunicator(
                "54.173.53.173", //host ip address
                "B50783", //username (your student code)
                "B50783", //password (your student code)
                22
        );

        cloudCommunicator.addListener(new CloudServiceCommunicator.SSHListener() {
            @Override
            public void commandResultReceived(String result) {
                Log.i(TAG, result);
            }

            @Override
            public void fileSent() {
                Log.i(TAG, "File sent");
            }
        });

        // Send a simple ECHO command just to see that we really are connected.
        String command = "echo --- CONNECTED TO: $(hostname)---;";
        cloudCommunicator.doSendCommandTask(command);

        //// Intent to choose photo
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, UNCOVER_PHOTO);
    }

    /**
     * Here we define the behaviour for sending the selected photo to the instance
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    String message = ((EditText) findViewById(R.id.et_message)).getText().toString();
                    String mFileName = "in.txt";
                    cacheMessage(mFileName, message);
                    Uri selectedImage = imageReturnedIntent.getData();
                    new ProcessImageTask().execute(selectedImage);
                }
            case UNCOVER_PHOTO:
                if (resultCode == RESULT_OK) {
                    String regID = ((EditText) findViewById(R.id.et_gcmid)).getText().toString();
                    String mFileName = "regid.txt";
                    cacheMessage(mFileName, regID);
                    Uri selectedImage = imageReturnedIntent.getData();
                    new UncoverImageTask().execute(selectedImage);
                }
        }
    }


    private class ProcessImageTask extends AsyncTask<Uri, String, Uri> {
        protected Uri doInBackground(Uri... imageuris) {
            String filename = Utils.getFileNameByUri(MainActivity.this, imageuris[0]);
            String mFileName = "in.txt";

            // 1. Send the image
            try {
                publishProgress("Sending image..");
                InputStream imageStream = getContentResolver().openInputStream(imageuris[0]);
                cloudCommunicator.sendFile(imageStream, filename);
            } catch (FileNotFoundException e) {
                publishProgress(e.getMessage());
            }

            // 1.2 Send the text
            try {
                publishProgress("Sending message..");
                FileInputStream fisMessage = openFileInput(mFileName);
                InputStream messageStream = fisMessage;
                cloudCommunicator.sendFile(messageStream, mFileName);
            } catch (FileNotFoundException e) {
                publishProgress(e.getMessage());
            }

            // 2. Apply command to the image remotely.
            publishProgress("Processing image..");
            String outputFilename = "covert_" + filename;
            String command = String.format("java -jar f5.jar e -e %s %s %s", mFileName, filename, outputFilename);
            cloudCommunicator.sendCommand(command);

            // 3. Retrieve the processed output image from remote
            publishProgress("Retrieving image..");
            cloudCommunicator.receiveFile(outputFilename, getExternalFilesDir(null));

            return Uri.fromFile(new File(getExternalFilesDir(null),outputFilename));
        }

        protected void onProgressUpdate(String... progress) {
            Toast.makeText(getApplicationContext(), progress[0], Toast.LENGTH_SHORT).show();
        }

        protected void onPostExecute(Uri result) {
            String filepath = getExternalFilesDir(null).getAbsolutePath() +
                    Utils.getFileNameByUri(getApplicationContext(), result);
            Toast.makeText(getApplicationContext(), "DONE! file stored to: "+filepath, Toast.LENGTH_LONG).show();
            showImage(result);
        }
    }

    private class UncoverImageTask extends AsyncTask<Uri, String, Uri> {
        protected Uri doInBackground(Uri... imageuris) {
            String filename = Utils.getFileNameByUri(MainActivity.this, imageuris[0]);
            String mFileName = "regid.txt";

            // 1. Send the image
            try {
                publishProgress("Sending image..");
                InputStream imageStream = getContentResolver().openInputStream(imageuris[0]);
                cloudCommunicator.sendFile(imageStream, filename);
            } catch (FileNotFoundException e) {
                publishProgress(e.getMessage());
            }

            // 1.2 Send the text
            try {
                publishProgress("Sending regid..");
                FileInputStream fisMessage = openFileInput(mFileName);
                InputStream messageStream = fisMessage;
                cloudCommunicator.sendFile(messageStream, mFileName);
            } catch (FileNotFoundException e) {
                publishProgress(e.getMessage());
            }

            // 2. Apply command to uncover the text remotely.
            publishProgress("Processing image..");
            String outputFilename = "out.txt";
            String command = String.format("java -jar f5.jar x -e %s %s", outputFilename, filename);
            cloudCommunicator.sendCommand(command);

            // 3. Apply command to send the message through GCM
            publishProgress("Sending message...");
            command = String.format("curl 'http://54.173.53.173:8080/gcm/SendMessage' --data 'regId='`tail %s`'&message='`tail %s`", mFileName, outputFilename);
            cloudCommunicator.sendCommand(command);
            // cloudCommunicator.receiveFile(outputFilename, getExternalFilesDir(null));

            return null; // Uri.fromFile(new File(getExternalFilesDir(null),outputFilename));
        }

        protected void onProgressUpdate(String... progress) {
            Toast.makeText(getApplicationContext(), progress[0], Toast.LENGTH_SHORT).show();
        }

        protected void onPostExecute(Uri result) {
            Toast.makeText(getApplicationContext(), "DONE! Message sent. ", Toast.LENGTH_LONG).show();
        }
    }



    /** Opens a pop-up dialog showing the image corresponding to the provided Uri */
    public void showImage(Uri imageUri) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);
        Bitmap scaled;
        //Scale down the bitmap to avoid bitmap memory errors, set it to the imageview.
        //This code scales it such that the width is 1280
        try {
            Bitmap bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(),imageUri);
            int nh = (int) ( bmp.getHeight() * (1280.0 / bmp.getWidth()) );
            scaled = Bitmap.createScaledBitmap(bmp, 1280, nh, true);
            imageView.setImageBitmap(scaled);
        } catch (IOException e) {
            e.printStackTrace();
        }

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    /**
     * Method to add the messages to the display textview
     * @param message
     */
    public void appendMessage(String message){
        t1.append("\n" + message);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "GCM Registration not found.");
            return "";
        }

        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }

        ((EditText) findViewById(R.id.et_gcmid)).setText(registrationId);

        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // Sunshine persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you. Just make sure
        // that it is private!
        return getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // Should never happen. WHAT DID YOU DO?!?!
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(context);
                    }
                    String regId = mGcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regId;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.

                    //sendRegistrationIdToBackend();
                    // For this demo: we are not doing this. Instead we are sending it to the server along with the message.

                    // Persist the registration ID - no need to register again.
                    storeRegistrationId(context, regId);
                    Log.i(TAG, "Registration ID " + regId);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // TODO: If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return null;
            }
        }.execute(null, null, null);
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        ((EditText) findViewById(R.id.et_gcmid)).setText(regId);
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Create text file with the message to send
     */
    public void cacheMessage(String FILENAME, String input){
        FileOutputStream fos;
        try {
            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(input.getBytes());
            Log.v(DEBUG_FILE_TAG, "Writing to " + FILENAME);
            fos.close();

        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}




