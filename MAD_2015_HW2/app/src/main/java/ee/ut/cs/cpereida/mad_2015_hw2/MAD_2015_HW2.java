package ee.ut.cs.cpereida.mad_2015_hw2;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MAD_2015_HW2 extends AppCompatActivity {

    // request code expected
    static final int REQUEST_CALL_GREETINGS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mad_2015__hw2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mad_2015__hw2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void send(View v) {
        // Log.v("send", "Start sending...");

        String email = ((EditText) findViewById(R.id.email)).getText().toString();
        String text =  getResources().getString(R.string.txt2);
        ((TextView) findViewById(R.id.txt2)).setText(text + " " + email);

        //Log.v("main", email);
        // create intent and send email to next activity
        Intent intent = new Intent(this, GreetingActivity.class);
        intent.putExtra("email", email);
        startActivityForResult(intent, REQUEST_CALL_GREETINGS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CALL_GREETINGS && resultCode == RESULT_OK) {
            // Everything fine
            Log.v("main", "Everything is fine!");
            // print "Email sent"
            ((TextView) findViewById(R.id.txt2)).setText("Email sent!");
        } else {
            Log.v("main", "Errors: " + Integer.toString(requestCode)
                    + " " + Integer.toString(resultCode));
            // Something went wrong
            ((TextView) findViewById(R.id.txt2)).setText("Something went wrong!");
        }
    }
}
