package ee.ut.cs.cpereida.mad_2015_hw2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class GreetingActivity extends AppCompatActivity {

    static final int REQUEST_SELECT_CONTACT = 1;
    static final int REQUEST_SEND_EMAIL = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);

        String email, title;
        // get Intent from main activity
        Intent intent = getIntent();
        email = intent.getStringExtra("email");
        title = getResources().getString(R.string.txt2);
        ((TextView) findViewById(R.id.tv1)).setText(title + " " + email + " or choose " +
                "someone else.");
        // put the email from main activity in edit text
        ((EditText) findViewById(R.id.email_et)).setText(email);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_greeting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendEmail(View v) {
        // Retrieve the values from the EditTexts
        String subject = ((EditText) findViewById(R.id.subject_et)).getText().toString();
        String body = ((EditText) findViewById(R.id.body_et)).getText().toString();
        String email = ((EditText) findViewById(R.id.email_et)).getText().toString();
        // Add extras to email
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        // Start activity with result for Gmail or Inbox
        startActivityForResult(Intent.createChooser(emailIntent, "Email:"), REQUEST_SEND_EMAIL);
    }

    public void pickContact(View v) {
        // Start an activity for the user to pick a phone number from contacts
        Log.v("greeting", "Inside pickContact");
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            Log.v("greeting", "starting activity");
            startActivityForResult(intent, REQUEST_SELECT_CONTACT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_CONTACT && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone contacts
            Uri contactUri = data.getData();
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS};
            Cursor cursor = getContentResolver().query(contactUri, projection,
                    null, null, null);
            // If the cursor returned is valid, get the email address
            if (cursor != null && cursor.moveToFirst()) {
                int addressIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
                String address = cursor.getString(addressIndex);
                // Put the email address
                ((EditText) findViewById(R.id.email_et)).setText(address);
            }
        }
        // result code from inbox/gmail is RESULT_CANCELED but mail is sent, just ignore.
        if (requestCode == REQUEST_SEND_EMAIL && resultCode == RESULT_OK ||
                requestCode == REQUEST_SEND_EMAIL && resultCode == RESULT_CANCELED) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            Log.v("greetings", Integer.toString(requestCode) + " " + Integer.toString(resultCode));
        }
    }

}
