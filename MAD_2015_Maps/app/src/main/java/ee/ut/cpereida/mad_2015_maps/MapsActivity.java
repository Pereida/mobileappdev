package ee.ut.cpereida.mad_2015_maps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private int MY_LOCATION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }



    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        // create Route object to draw the route
        final Route route = new Route();
        // set the coordinates for the CSBuilding and yourself
        final LatLng CSBUILDING = new LatLng(58.3783114, 26.7145928);
        // create Marker for the CSBuilding
        Marker csbuilding = mMap.addMarker(new MarkerOptions()
                .position(CSBUILDING)
                .draggable(false)
                .title("Computer Science Dpt."));

        // check Permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        // Define a listener that responds to location updates
        final LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                Log.v("Location Listener", "GPS onLocationChanged " + location);
                // Called when a new location is found by the network location provider.

                String url;
                // Get our coordinates
                LatLng MYPOSITION = new LatLng(location.getLatitude(), location.getLongitude());
                Marker myposition = mMap.addMarker(new MarkerOptions()
                        .position(MYPOSITION)
                        .draggable(false)
                        .title("You are here!."));

                // set the camera zoom to a reasonable zoom
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MYPOSITION, 15));
                // draw the route to the University
                route.drawRoute(mMap, MapsActivity.this, MYPOSITION, CSBUILDING, "walking", true, "en");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);


        // user has maybe disabled location services / GPS
        if (locationManager.getProviders(true).size() == 0) {
            Toast.makeText(this, "Cannot get location, no location providers enabled. Check device " +
                    "settings", Toast.LENGTH_LONG).show();
        }

        // use all enabled device providers with same parameters
        for (String provider : locationManager.getProviders(true)) {
            Log.v("Location Provider", "adding location provider " + provider);
            locationManager.requestLocationUpdates(provider, 1000, 50, locationListener);
        }

    }
}





