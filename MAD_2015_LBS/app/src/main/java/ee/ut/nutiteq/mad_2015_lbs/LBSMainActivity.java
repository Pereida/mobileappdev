package ee.ut.nutiteq.mad_2015_lbs;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.nutiteq.core.MapPos;
import com.nutiteq.core.MapRange;
import com.nutiteq.datasources.LocalVectorDataSource;
import com.nutiteq.geometry.GeoJSONGeometryReader;
import com.nutiteq.geometry.Geometry;
import com.nutiteq.graphics.Color;
import com.nutiteq.layers.NutiteqOnlineVectorTileLayer;
import com.nutiteq.layers.VectorLayer;
import com.nutiteq.layers.VectorTileLayer;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.styles.BalloonPopupStyleBuilder;
import com.nutiteq.styles.MarkerStyle;
import com.nutiteq.styles.MarkerStyleBuilder;
import com.nutiteq.styles.PolygonStyle;
import com.nutiteq.styles.PolygonStyleBuilder;
import com.nutiteq.ui.MapClickInfo;
import com.nutiteq.ui.MapEventListener;
import com.nutiteq.ui.MapView;
import com.nutiteq.ui.VectorElementClickInfo;
import com.nutiteq.ui.VectorElementsClickInfo;
import com.nutiteq.utils.Log;
import com.nutiteq.vectorelements.BalloonPopup;
import com.nutiteq.vectorelements.Billboard;
import com.nutiteq.vectorelements.Marker;
import com.nutiteq.vectorelements.Polygon;
import com.nutiteq.vectorelements.VectorElement;
import com.nutiteq.wrappedcommons.MapPosVector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

public class LBSMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lbsmain);

        // 1. The initial step: register your license. This must be done before using MapView!
        MapView.registerLicense("XTUMwQ0ZIcTNrcUhpcHZjeHJ3MVhLNFdmSGJOVlZ6WUNBaFVBZ29BMWZVTUE4UUNMeWVxZmRLTkd0QUFudkQwPQoKcHJvZHVjdHM9c2RrLWFuZHJvaWQtMy4qLHNkay1pb3MtMy4qCnBhY2thZ2VOYW1lPWVlLnV0Lm51dGl0ZXEuKgp3YXRlcm1hcms9bnV0aXRlcQp2YWxpZFVudGlsPTIwMTUtMTEtMDEKdXNlcktleT0yZjFmM2JlYjQ1NmU4YTZhNWE5Nzk4NWE4NTgyY2U0ZAo=", getApplicationContext());

        // Create map view
        final MapView mapView = (MapView) this.findViewById(R.id.mapView);

        // Create base layer. Use vector style from assets
        VectorTileLayer baseLayer = new NutiteqOnlineVectorTileLayer("nutibright-v2.zip");

        // Add layer to map
        mapView.getLayers().add(baseLayer);

        // 2. Add a pin marker to map
        // Initialize a local vector data source
        final Projection proj = mapView.getOptions().getBaseProjection();
        final LocalVectorDataSource vectorDataSource = new LocalVectorDataSource(proj);

        // Create marker style, by first loading marker bitmap
        MarkerStyleBuilder markerStyleBuilder = new MarkerStyleBuilder();
        markerStyleBuilder.setSize(30);
        MarkerStyle sharedMarkerStyle = markerStyleBuilder.buildStyle();
        // Add marker to the local data source
        MapPos mapPos = proj.fromWgs84(new MapPos(13.38933, 52.51704));
        final Marker marker1 = new Marker(mapPos, sharedMarkerStyle);

        // for clicking
        marker1.setMetaDataElement("name","My Location");

        vectorDataSource.add(marker1);

        // Create a vector layer with the previously created data source
        VectorLayer vectorLayer1 = new VectorLayer(vectorDataSource);
        // Add the previous vector layer to the map
        mapView.getLayers().add(vectorLayer1);

        // listener, use in-line class definition here
        // in real app use separate class


        mapView.setMapEventListener(new MapEventListener()
                                    {
                                        public BalloonPopup visibleClickLabel;

                                        @Override
                                        public void onMapMoved() {

                                        }

                                        @Override
                                        public void onMapClicked(MapClickInfo mapClickInfo) {
                                            Log.debug("onMapClicked "+mapClickInfo.getClickPos());
                                        }

                                        @Override
                                        public void onVectorElementClicked(VectorElementsClickInfo vectorElementsClickInfo) {

                                            // Remove old click label
                                            if (visibleClickLabel != null) {
                                                vectorDataSource.remove(visibleClickLabel);
                                                visibleClickLabel = null;
                                            }

                                            // Multiple vector elements can be clicked at the same time, we only care about the one
                                            // closest to the camera
                                            VectorElementClickInfo clickInfo = vectorElementsClickInfo.getVectorElementClickInfos().get(0);
                                            Log.debug("onVectorElementClicked " + clickInfo.getClickPos());

                                            BalloonPopupStyleBuilder styleBuilder = new BalloonPopupStyleBuilder();
                                            styleBuilder.setPlacementPriority(10);

                                            VectorElement vectorElement = clickInfo.getVectorElement();

                                            // click on popup - ignore
                                            if(vectorElement instanceof BalloonPopup)
                                                return;

                                            String clickText = vectorElement.getMetaDataElement("name");
                                            String clicDesc = vectorElement.getMetaDataElement("description");

                                            // Attach the click label to the billboard element
                                            BalloonPopup clickPopup = new BalloonPopup((Billboard) vectorElement,
                                                    styleBuilder.buildStyle(),
                                                    clickText,
                                                    clicDesc);
                                            vectorDataSource.add(clickPopup);
                                            visibleClickLabel = clickPopup;

                                        }
                                    }

        );


        // Set view
        mapView.setFocusPos(proj.fromWgs84(new MapPos(13.38933, 52.51704)), 0);
        mapView.setZoom(10, 0); // zoom 2, duration 0 seconds (no animation)
        mapView.setMapRotation(0, 0);
        mapView.setTilt(90, 0);

        // Some options
        mapView.getOptions().setRotatable(false); // make map not rotatable
        mapView.getOptions().setTileThreadPoolSize(4); // faster tile downloading

        mapView.getOptions().setZoomRange(new MapRange(3, 20));



        // style for GPS My Location circle
        PolygonStyleBuilder polygonStyleBuilder = new PolygonStyleBuilder();
        polygonStyleBuilder.setColor(new Color(0xAAFF0000));
        PolygonStyle gpsStyle = polygonStyleBuilder.buildStyle();

        MapPosVector gpsCirclePoses = new MapPosVector();
        final Polygon locationCircle = new Polygon(gpsCirclePoses, gpsStyle);
        // initially empty and invisible
        locationCircle.setVisible(false);

        vectorDataSource.add(locationCircle);


        final LocationListener locationListener = new LocationListener(){

            @Override
            public void onLocationChanged(Location location) {
                Log.debug("GPS onLocationChanged "+location);
                if (locationCircle != null) {
                    locationCircle.setPoses(createLocationCircle(location, proj));
                    locationCircle.setVisible(true);
                    mapView.setFocusPos(proj.fromWgs84(new MapPos(location.getLongitude(), location.getLatitude())), 0.5f);
                    marker1.setPos(proj.fromWgs84(new MapPos(location.getLongitude(), location.getLatitude())));
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {}

            @Override
            public void onProviderEnabled(String s) {}

            @Override
            public void onProviderDisabled(String s) {}
        };

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // user has maybe disabled location services / GPS
        if(locationManager.getProviders(true).size() == 0){
            Toast.makeText(this, "Cannot get location, no location providers enabled. Check device settings",Toast.LENGTH_LONG).show();
        }

        // use all enabled device providers with same parameters
        for(String provider : locationManager.getProviders(true)){
            Log.debug("adding location provider " + provider);
            locationManager.requestLocationUpdates(provider, 1000, 50, locationListener);
        }

        new CartoDbRequestTask(vectorDataSource).execute("SELECT sc.*,ay.animi,ay.mnimi,ay.onimi FROM ee_speedcams sc, asustusyksus_20120701 ay where st_contains(ay.the_geom,sc.the_geom)");
    }


    class CartoDbRequestTask extends AsyncTask<String, String, String> {


        private final LocalVectorDataSource vectorDataSource;

        public CartoDbRequestTask(LocalVectorDataSource vectorDataSource) {
            this.vectorDataSource = vectorDataSource;
        }


        @Override
        protected String doInBackground(String... qry) {
            String responseString = "";

            try {

                URL url = new URL("https://nutiteq.cartodb.com/api/v2/sql?format=GeoJSON&q=" + URLEncoder.encode(qry[0], "utf-8"));

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String inLine;
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(in));

                while ((inLine = reader.readLine()) != null) {
                    responseString += inLine;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // read GeoJSON format
            // parse FeatureCollection as normal JSON, then use SDK parser for GeoJSON geometries

            try {

                JSONObject json = new JSONObject(result);

                GeoJSONGeometryReader geoJsonParser = new GeoJSONGeometryReader();
                geoJsonParser.setTargetProjection(new EPSG3857());

                MarkerStyleBuilder markerStyleBuilder = new MarkerStyleBuilder();
                markerStyleBuilder.setColor(new Color(android.graphics.Color.RED));

                JSONArray features = json.getJSONArray("features");
                for (int i = 0; i < features.length(); i++) {
                    JSONObject feature = (JSONObject) features.get(i);
                    JSONObject geometry = feature.getJSONObject("geometry");

                    // use SDK GeoJSON parser
                    Geometry ntGeom = geoJsonParser.readGeometry(geometry.toString());

                    JSONObject properties = feature.getJSONObject("properties");

                    // create Marker for each object
                    Marker popup = new Marker(
                            ntGeom,
                            markerStyleBuilder.buildStyle());

                    // add all properties as MetaData, so you can use it with click handling
                    for (Iterator<String> j = properties.keys(); j.hasNext(); ) {
                        String key = j.next();
                        String val = properties.getString(key);
                        popup.setMetaDataElement(key, val);
                    }

                    // set MetaData for click popup and actions
                    popup.setMetaDataElement("name", properties.getString("description"));
                    popup.setMetaDataElement("description", properties.getString("type") + ". " + properties.getString("animi") + ", " + properties.getString("mnimi"));


                    vectorDataSource.add(popup);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private MapPosVector createLocationCircle(Location location, Projection proj) {

        // number of points of circle
        int	N = 50;
        int EARTH_RADIUS = 6378137;

        float radius = location.getAccuracy();
        double centerLat = location.getLatitude();
        double centerLon = location.getLongitude();

        MapPosVector points = new MapPosVector();
        for (int i = 0; i <= N; i++)
        {
            double angle = Math.PI*2*i/N;
            double dx = radius*Math.cos(angle);
            double dy = radius*Math.sin(angle);
            double lat = centerLat + (180/Math.PI) * (dy/ EARTH_RADIUS);
            double lon = centerLon + (180/Math.PI) * (dx/ EARTH_RADIUS)/Math.cos(centerLat * Math.PI/180);
            points.add(proj.fromWgs84(new MapPos(lon, lat)));
        }
        return points;
    }
}
