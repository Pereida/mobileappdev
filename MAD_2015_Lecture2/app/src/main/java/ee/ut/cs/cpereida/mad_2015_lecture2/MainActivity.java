package ee.ut.cs.cpereida.mad_2015_lecture2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_flags);
        setContentView(R.layout.activity_2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void send(View v) {
        Log.v("send", "Inside OnClick");
        int celsius;
        float fahrenheit;

        EditText et_celsius = (EditText) findViewById(R.id.celsius);
        EditText et_fahr = (EditText) findViewById(R.id.fahrenheit);

        celsius = Integer.parseInt(et_celsius.getText().toString());
        fahrenheit = (9.0f * (celsius/5.0f)) + 32;
        Log.v("send", Float.toString(fahrenheit));
        et_fahr.setText(Float.toString(fahrenheit));

        Intent intent = new Intent(this, greeting.class);
        intent.putExtra("fahrenheit", fahrenheit);
        startActivity(intent);
    }
}
