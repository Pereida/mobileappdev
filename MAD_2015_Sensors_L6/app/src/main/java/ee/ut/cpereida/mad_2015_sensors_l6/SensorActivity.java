package ee.ut.cpereida.mad_2015_sensors_l6;

import android.app.ActionBar;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class SensorActivity extends AppCompatActivity implements SensorEventListener {

    TextView tv1 = null;
    TextView block_l = null;
    TextView block_r = null;
    TextView block_m = null;
    private SensorManager mSensorManager;
    private Sensor mAcc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

//        tv1 = (TextView) findViewById(R.id.tv1);
        block_l = (TextView) findViewById(R.id.block_left);
        block_r = (TextView) findViewById(R.id.block_right);
        block_m = (TextView) findViewById(R.id.block_mid);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

//        List<Sensor> mList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

//        for (int i = 1; i < mList.size(); i++) {
//            tv1.append("\n" + mList.get(i).getName());
//        }


    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public final void onSensorChanged(SensorEvent event) {

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

//        tv1.setText("X: "+x+" Y: "+y+" Z: "+z);
        if (x > 4.5) {
            block_l.setAlpha(0);
            block_m.setAlpha(1);
            block_r.setAlpha(1);
        } else if (x < -4.5) {
            block_l.setAlpha(1);
            block_m.setAlpha(1);
            block_r.setAlpha(0);
        } else {
            block_m.setAlpha(0);
            block_l.setAlpha(1);
            block_r.setAlpha(1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause(){
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
