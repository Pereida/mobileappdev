package ee.ut.cpereida.mad_2015_homeassignment2;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements SensorEventListener {

    CustomDrawableView mCustomDrawableView = null;
    ShapeDrawable mDrawable = new ShapeDrawable();
    public float xPosition, xAcceleration,xVelocity = 0.0f;
    public float yPosition, yAcceleration,yVelocity = 0.0f;
    public float xmax,ymax,ymin;
    private Bitmap mBitmap;
    private Bitmap mWood;
    private Bitmap mChair;
    private SensorManager sensorManager = null;
    public float frameTime = 0.333f;
    private float speed = 10.0f;
    private DisplayMetrics displaymetrics = new DisplayMetrics();
    // Ball width and height
    private int ballHeight = 150;
    private int ballWidth = 150;
    // chair position and size
    private int chairHeight = 300;
    private int chairWidth = 500;
    private int chairXPos, chairYPos;
    // background size
    private int backWidth, backHeight;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        // Get ball standard position
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        backHeight = displaymetrics.heightPixels;
        backWidth = displaymetrics.widthPixels;

        chairXPos = backWidth / 5 * 2;
        chairYPos = backHeight / 3 * 2;

        yAcceleration = 5.0f;

        //Set FullScreen & portrait
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Get a reference to a SensorManager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);

        mCustomDrawableView = new CustomDrawableView(this);
        setContentView(mCustomDrawableView);
        // setContentView(R.layout.main);

        //Calculate Boundry
        Display display = getWindowManager().getDefaultDisplay();
        xmax = (float)display.getWidth() - ballHeight;
        ymax = backHeight / 4 * 3;
        ymin = backHeight / 5 * 1;
    }

    // This method will update the UI on new sensor events
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            //Set sensor values as acceleration
            //yAcceleration = sensorEvent.values[0];
            //Log.v("yAcceleration", Float.toString(yAcceleration));
            xAcceleration = sensorEvent.values[1];
            updateBall();
        }
    }

    private void updateBall()
    {
        //Calculate new speed
        xVelocity += (xAcceleration * frameTime);
        //Calc distance travelled in that time
        float xS = (xVelocity / 2) * frameTime;
        //Add to position due to sensor
        xPosition += xS;

        yVelocity += (yAcceleration * frameTime);
        float yS = (yVelocity/2)*frameTime;
        yPosition += yS;

        // reset velocity when reaching boundaries
        if (xPosition > xmax ) {
            xPosition = xmax;
            xVelocity = 0;
        } else if (xPosition < 0) {
            xPosition = 0;
            xVelocity = 0;
        } else if (xPosition > chairXPos && xPosition < chairXPos+chairWidth && yPosition > chairYPos) {
            xVelocity = 0;
            xAcceleration = 0;
        }

        if (yPosition > ymax) {
            yPosition = ymax;
            yVelocity = 0;
        } else if (yPosition <= ymin) {
            yPosition = ymin;
            yVelocity = 0;
            yAcceleration = speed;
        }
    }

    // I've chosen to not implement this method
    public void onAccuracyChanged(Sensor arg0, int arg1)
    {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop()
    {
        // Unregister the listener
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    public class CustomDrawableView extends View
    {
        public CustomDrawableView(Context context)
        {
            super(context);
            // create bitmap for the ball and the background
            Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
            Bitmap wood = BitmapFactory.decodeResource(getResources(), R.drawable.wood);
            Bitmap chair = BitmapFactory.decodeResource(getResources(), R.drawable.chair);
            // set sizes
            mBitmap = Bitmap.createScaledBitmap(ball, ballWidth, ballHeight, true);
            mWood = Bitmap.createScaledBitmap(wood, backWidth, backHeight, true);
            mChair = Bitmap.createScaledBitmap(chair, chairWidth, chairHeight, true);
        }

        protected void onDraw(Canvas canvas)
        {
            final Bitmap bitmap = mBitmap;
            canvas.drawBitmap(mWood, 0, 0, null);
            canvas.drawBitmap(mChair, chairXPos, chairYPos, null);
            canvas.drawBitmap(bitmap, xPosition, yPosition, null);
            invalidate();
        }

        public boolean onTouchEvent(MotionEvent event){
            // Log.v("Event", Integer.toString(event.getAction()));
            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    // Log.v("DOWN", "DOWN");
                    yAcceleration = -speed;
                    break;
            }
            return true;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
}