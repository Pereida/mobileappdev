package ee.ut.cs.cpereida.mad_2015_lecture3;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private static String DEBUG_FILE_TAG = "Writing";
    private static String DEBUG_CONTENT_TAG = "CONTENT_PROVIDERS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Writing to a File and Reading to a file
        /*String FILENAME = "text.txt";
        String input = "moi world!";

        FileOutputStream fos;
        FileInputStream fis;
        try {
            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(input.getBytes());
            Log.v(DEBUG_FILE_TAG, "Writing to " + FILENAME);
            fos.close();
            fis = openFileInput(FILENAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String out = br.readLine();

            if (out.equalsIgnoreCase(input))
            {
                // log
                Log.v(DEBUG_FILE_TAG, out + " and " + input + " are the same");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // HUOM! Program crashes if emulator does not have contacts!!!
        Cursor people = getContacts();

        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        do {
            String name = people.getString(indexName);
            String number = people.getString(indexNumber);

            Log.v(DEBUG_CONTENT_TAG, "Name: " + name + " Phone: " + number);
        } while (people.moveToNext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Cursor getContacts() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[] {
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        Cursor people = getContentResolver().query(uri, projection, null, null, null);

        return people;
    }
}
