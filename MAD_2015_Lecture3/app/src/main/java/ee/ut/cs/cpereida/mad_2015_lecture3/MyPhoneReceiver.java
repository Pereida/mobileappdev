package ee.ut.cs.cpereida.mad_2015_lecture3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

public class MyPhoneReceiver extends BroadcastReceiver {

    public static String DEBUG_TAG = "MyPhoneReceiver";

    public MyPhoneReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        // throw new UnsupportedOperationException("Not yet implemented");

        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            String state = extras.getString(TelephonyManager.EXTRA_STATE);
            Log.w(DEBUG_TAG, state);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING))
            {
                String phoneNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.w(DEBUG_TAG, phoneNumber);
            }
        }
    }
}
