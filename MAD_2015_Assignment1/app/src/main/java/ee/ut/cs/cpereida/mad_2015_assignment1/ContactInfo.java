package ee.ut.cs.cpereida.mad_2015_assignment1;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ContactInfo extends AppCompatActivity {

    static int REQUEST_SEND_EMAIL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);

        String name, phone, email;
        // get Intent from main activity
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");

        // put the email from main activity in edit text
        ((TextView) findViewById(R.id.name_tv)).setText(name);
        ((TextView) findViewById(R.id.phone_tv)).setText(phone);
        ((TextView) findViewById(R.id.email_tv)).setText(email);
        ((EditText) findViewById(R.id.email_et)).setText(email);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendEmail(View v) {
        // Retrieve the values from the EditTexts
        String subject = ((EditText) findViewById(R.id.subject_et)).getText().toString();
        String body = ((EditText) findViewById(R.id.body_et)).getText().toString();
        String email = ((EditText) findViewById(R.id.email_et)).getText().toString();

        // Add extras to email
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);

        // Start activity with result for Gmail or Inbox
        startActivityForResult(Intent.createChooser(emailIntent, "Email:"), REQUEST_SEND_EMAIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // result code from inbox/gmail is RESULT_CANCELED but mail is sent, just ignore.
        if (requestCode == REQUEST_SEND_EMAIL && resultCode == RESULT_OK ||
                requestCode == REQUEST_SEND_EMAIL && resultCode == RESULT_CANCELED) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            Log.v("greetings", Integer.toString(requestCode) + " " + Integer.toString(resultCode));
        }
    }
}
