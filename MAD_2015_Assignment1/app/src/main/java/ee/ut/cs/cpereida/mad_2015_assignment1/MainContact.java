package ee.ut.cs.cpereida.mad_2015_assignment1;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainContact extends AppCompatActivity {

    static String MAIN_CONTACT_DEBUG_TAG = "DebugContact";
    static int REQUEST_CALL_CONTACT_INFO = 1;

    ListView myListview;
    ArrayList<String> contacts = new ArrayList<String>();
    String myQuery = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_contact);

        getContacts(null);

    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate(R.menu.menu_main_contact, menu);

        // Add SearchWidget.
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem( R.id.action_search ).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast.makeText(MainContact.this ,"Our word : " + query, Toast.LENGTH_SHORT).show();
                // Query database only on submit
                // myQuery = query;
                // getContacts(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // Query database every text change, resource consuming but looks nicer
                myQuery = query;
                getContacts(query);
                return false;
            }
        });
        searchView.setQuery(myQuery, true);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {

        super.onSaveInstanceState(bundle);

        bundle.putString("query", myQuery);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Bundle bundle = savedInstanceState;
            this.myQuery = bundle.getString("query");
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void getContacts(String query) {
        if (query != null && !query.isEmpty())
            Log.v(MAIN_CONTACT_DEBUG_TAG, query);

        // Retrieve ListView from layout
        myListview = (ListView) findViewById(R.id.contact_lv);
        // create ArrayList of contacts
        contacts = new ArrayList<String>();

        Cursor nameCursor;  // Cursor object
        String name;    // Contact name
        int nameIdx;    // Index of DISPLAY_NAME column

        // Get the name
        // Uri
        Uri nameUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        // Projection
        String[] nameProjection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
        // Sorting
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " DESC";

        if (query != null && !query.isEmpty()) {
            nameCursor = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE ?",
                    new String[]{"%" + query + "%"}, null);
            Log.v(MAIN_CONTACT_DEBUG_TAG, "Inside If");
        }
        else {
            nameCursor = getContentResolver().query(nameUri, nameProjection, null, null, sortOrder);
            Log.v(MAIN_CONTACT_DEBUG_TAG, "Else");
        }

        if (nameCursor.moveToFirst()) {
            do {
                nameIdx = nameCursor.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                name = nameCursor.getString(nameIdx);
                Log.v(MAIN_CONTACT_DEBUG_TAG, name);
                contacts.add(nameIdx, name);


            } while (nameCursor.moveToNext());
        }

        // create ArrayAdapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, contacts);
        // set the Adapter to the ListView
        myListview.setAdapter(adapter);

        // Create a message handling object as an anonymous class.
        AdapterView.OnItemClickListener mMessageClickedHandler =
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id) {
                        // Do something in response to the click
                        /*Toast.makeText(getApplicationContext(),
                                ((String) parent.getItemAtPosition(position)),
                                Toast.LENGTH_LONG).show();*/
                        String[] list;
                        String name = (String) parent.getItemAtPosition(position);
                        list = getContactInfo(name);
                        // Log.v(MAIN_CONTACT_DEBUG_TAG, Arrays.toString(list));

                        Intent intent = new Intent(MainContact.this, ContactInfo.class);
                        intent.putExtra("name", list[0]);

                        if (list[1] != null)
                            intent.putExtra("phone", list[1]);
                        else
                            intent.putExtra("phone", "No phone information.");

                        if (list[2] != null)
                            intent.putExtra("email", list[2]);
                        else
                            intent.putExtra("email", "No email information.");

                        startActivityForResult(intent, REQUEST_CALL_CONTACT_INFO);
                    }
                };


        myListview.setOnItemClickListener(mMessageClickedHandler);

    }

    private String[] getContactInfo(String name) {
        String mime;    // MIME type
        String email, phone;    // Contact name
        int dataIdx;    // Index of DATA1 column
        int mimeIdx;    // Index of MIMETYPE column

        String[] list = new String[3];
        list[0] = name;

        Uri uri = ContactsContract.Data.CONTENT_URI;

        // Set up the projection
        String[] projection = {
                        ContactsContract.Data.DISPLAY_NAME,
                        ContactsContract.Contacts.Data.DATA1,
                        ContactsContract.Contacts.Data.MIMETYPE};

        // Query ContactsContract.Data
        Cursor cursor = getContentResolver().query(uri, projection,
                        ContactsContract.Data.DISPLAY_NAME + " = ?",
                        new String[]{name},
                        null);

        if (cursor.moveToFirst()) {
            // Get the indexes of the MIME type and data
            mimeIdx = cursor.getColumnIndex(
                    ContactsContract.Contacts.Data.MIMETYPE);
            dataIdx = cursor.getColumnIndex(
                    ContactsContract.Contacts.Data.DATA1);

            // Match the data to the MIME type, store in variables
            do {
                mime = cursor.getString(mimeIdx);
                if (ContactsContract.CommonDataKinds.Email
                        .CONTENT_ITEM_TYPE.equalsIgnoreCase(mime)) {
                    email = cursor.getString(dataIdx);
                    if (email != null && !email.isEmpty()) {
                        list[2] = email;
                        //Log.v(MAIN_CONTACT_DEBUG_TAG, email);
                    }
                    else
                        list[2] = "No email details.";

                }
                if (ContactsContract.CommonDataKinds.Phone
                        .CONTENT_ITEM_TYPE.equalsIgnoreCase(mime)) {
                    phone = cursor.getString(dataIdx);
                    if (phone != null && !phone.isEmpty()) {
                        list[1] = phone;
                        //Log.v(MAIN_CONTACT_DEBUG_TAG, phone);
                    }
                    else
                        list[1] = "No phone details.";

                }
            } while (cursor.moveToNext());
        }

        return list;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CALL_CONTACT_INFO && resultCode == RESULT_OK) {
            // Everything fine
            Log.v("main", "Everything is fine!");
            // print "Email sent"
            Toast.makeText(getApplicationContext(),
                    "Email sent.",
                    Toast.LENGTH_LONG).show();
        } else {
            Log.v("main", "Errors: " + Integer.toString(requestCode)
                    + " " + Integer.toString(resultCode));
            // Something went wrong
            /*Toast.makeText(getApplicationContext(),
                    "Something went wrong.",
                    Toast.LENGTH_LONG).show();*/
        }
    }

}
