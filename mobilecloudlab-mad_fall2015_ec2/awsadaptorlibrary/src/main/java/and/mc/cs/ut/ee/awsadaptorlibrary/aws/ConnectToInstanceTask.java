package and.mc.cs.ut.ee.awsadaptorlibrary.aws;

import android.os.AsyncTask;

import com.amazonaws.services.ec2.model.Instance;

import and.mc.cs.ut.ee.awsadaptorlibrary.AwsInterface;
import and.mc.cs.ut.ee.awsadaptorlibrary.Utils;


public class ConnectToInstanceTask extends AsyncTask<String, String,Instance> {
    private final InstanceController instanceController;
    //    private final InstanceConnector connector;
    AwsInterface uiInterface;

    public ConnectToInstanceTask(InstanceController instanceController, AwsInterface uiInterface) {
        this.instanceController = instanceController;
        this.uiInterface = uiInterface;
	}

	@Override
	protected Instance doInBackground(String... instanceIds ) {
        Instance instance = null;
        try {
            instance = instanceController.connectToInstance(instanceIds[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return instance;
	}
    @Override
    protected void onProgressUpdate(String... progress) {
        if(progress[0]==null) progress[0] = "null";
        uiInterface.displayMessage(String.format("Waiting for instance to get ready;" +
                "\n instance state = %s", progress[0]));
    }

    @Override
	protected void onPostExecute(Instance result) {
		if (result == null){
            uiInterface.showError("Instance connection failed.");
		} else {
            String publicIP = result.getPublicIpAddress();
            uiInterface.displayMessage(String.format(
                    "Restored connection to instance, " +
                            "\n ip = '%s'" +
                            "\n id = '%s'." +
                            "\n AMI ID = '%s'", publicIP, result.getInstanceId(), result.getImageId()));

//            uiInterface.onInstanceUpdate(result, Utils.INSTANCE_RUNNING);
		}
	}
}
