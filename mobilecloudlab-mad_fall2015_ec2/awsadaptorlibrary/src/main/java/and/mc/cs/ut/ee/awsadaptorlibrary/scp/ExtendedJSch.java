package and.mc.cs.ut.ee.awsadaptorlibrary.scp;

import android.content.res.AssetManager;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Jakob on 7.10.2014.
 */
public class ExtendedJSch extends JSch {
    private static final String DEFAULT_KEYFILE_IDENTITY = "key_identity";

    public ExtendedJSch() {
        super();
    }

    public void setIdentityKey(InputStream keyFileInputStream) throws IOException, JSchException {
        InputStream input = keyFileInputStream;
        int size = input.available();
        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();

        addIdentity(DEFAULT_KEYFILE_IDENTITY, buffer, null, null);
        setConfig("StrictHostKeyChecking", "no");
    }

    public void setIdentityKey(AssetManager manager, String keyFileName) throws IOException, JSchException {
        InputStream input = manager.open(keyFileName);
        int size = input.available();
        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();

        addIdentity(keyFileName, buffer, null, null);
        setConfig("StrictHostKeyChecking", "no");
    }
}
