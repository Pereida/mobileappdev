package and.mc.cs.ut.ee.awsadaptorlibrary.scp;

import android.app.Activity;
import android.util.Log;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;

public class SshClient {

    final String TAG = getClass().getName();
    ExtendedJSch jsch;
    Activity activity;
    private String username;
    private String host;
    private String keyFileName;

    /**
     *
     * @param activity
     * @param keyFileName - must be contained in assets tied to activity
     */
    public SshClient(Activity activity, String keyFileName) {
        this.activity = activity;
        this.keyFileName = keyFileName;
        this.jsch = new ExtendedJSch();
    }

    protected void connectAndRunCommand(String username, String host, int port,  String command) throws Exception {
        Log.i(TAG, "starting session connect");


        jsch.setIdentityKey(activity.getAssets(), keyFileName);

        Session session = jsch.getSession(username, host, port);
        session.connect();


        //String command = "whoami;hostname";

        ChannelExec channel = (ChannelExec)session.openChannel("exec");
        Log.i(TAG, "starting channel connect2");
        channel.setCommand(command);
        channel.connect();

        readAndLogResponse(channel);

        channel.disconnect();
        session.disconnect();
    }

    private void readAndLogResponse(final ChannelExec channel) throws IOException, InterruptedException {
        InputStream input = channel.getInputStream();

        //start reading the input from the executed commands on the shell
        byte[] tmp = new byte[1024];
        Thread.sleep(1000);
        while (input.available() > 0) {
            int i = input.read(tmp, 0, 1024);
            if (i < 0) break;
            final String response = new String(tmp, 0, i);
            Log.i(TAG, response);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //!TODO Direct response to somewhere
//                    activity.displayMessage(response);
                }
            });

        }
        if (channel.isClosed()){
            final int exitStatus = channel.getExitStatus();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //!TODO Direct response to somewhere
                    //activity.displayMessage("exit-status: " + exitStatus);
                }
            });

        }
    }
}
