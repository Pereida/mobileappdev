package ee.ut.cs.cpereida.mad_2015_hw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText state1 = (EditText) findViewById(R.id.state1);
        String str = getResources().getString(R.string.oncreate);
        state1.setText(str);

    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        TextView txt1 = (TextView) findViewById(R.id.txt1);
        String str = getResources().getString(R.string.onpause);
        txt1.setText(str);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        EditText state2 = (EditText) findViewById(R.id.state2);
        String str = getResources().getString(R.string.onresume);
        state2.setText(str);

        TextView orientation = (TextView) findViewById(R.id.orientation);
        String orient;

        switch (getResources().getConfiguration().orientation) {
            case 1:
                orient = getResources().getString(R.string.portrait);
                orientation.setText(orient);
                break;
            case 2:
                orient = getResources().getString(R.string.landscape);
                orientation.setText(orient);
                break;
            default:
                orient = getResources().getString(R.string.unknown);
                orientation.setText(orient);
                break;
        }

        ShowPopUp popUp = new ShowPopUp();
    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first
        TextView txt2 = (TextView) findViewById(R.id.txt2);
        String str = getResources().getString(R.string.onstop);
        txt2.setText(str);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        String bye = getResources().getString(R.string.bye);
        TextView txt1 = (TextView) findViewById(R.id.txt1);
        TextView txt2 = (TextView) findViewById(R.id.txt2);
        TextView txt3 = (TextView) findViewById(R.id.orientation);
        EditText et1 = (EditText) findViewById(R.id.state1);
        EditText et2 = (EditText) findViewById(R.id.state2);

        txt1.setText(bye);
        txt2.setText(bye);
        txt3.setText(bye);
        et1.setText(bye);
        et2.setText(bye);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
