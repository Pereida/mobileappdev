package ee.ut.cs.cpereida.mad_2015_hw3;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class MPlayer extends Service {

    static int ONGOING_NOTIFICATION_ID = 1;

    public MPlayer() {
    }

    @Override
    public void onCreate() {
        audioPlayer();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Intent notificationIntent = new Intent(this, MPlayer.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = builder.setContentIntent(pendingIntent)
                                .setSmallIcon(R.drawable.ic_launcher)
                                .setContentTitle(getText(R.string.title))
                                .setContentText(getText(R.string.app_name)).build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
        stopForeground(true);
    }

    public void audioPlayer(){

        //set up MediaPlayer
        MediaPlayer mp = MediaPlayer.create(this, R.raw.chan_chan);

        try {
            // mp.setDataSource(path + File.separator + fileName);
            // mp.setDataSource();
            // mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
