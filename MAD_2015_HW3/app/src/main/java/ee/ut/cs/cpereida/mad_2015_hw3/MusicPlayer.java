package ee.ut.cs.cpereida.mad_2015_hw3;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;

import java.io.File;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MusicPlayer extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "ee.ut.cs.cpereida.mad_2015_hw3.action.FOO";
    private static final String ACTION_BAZ = "ee.ut.cs.cpereida.mad_2015_hw3.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "ee.ut.cs.cpereida.mad_2015_hw3.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "ee.ut.cs.cpereida.mad_2015_hw3.extra.PARAM2";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, MusicPlayer.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, MusicPlayer.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public MusicPlayer() {
        super("MusicPlayer");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            audioPlayer();
        }
    }


    public void audioPlayer(){

        //set up MediaPlayer
        MediaPlayer mp = MediaPlayer.create(this, R.raw.chan_chan);

        try {
            // mp.setDataSource(path + File.separator + fileName);
            // mp.setDataSource();
            // mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
