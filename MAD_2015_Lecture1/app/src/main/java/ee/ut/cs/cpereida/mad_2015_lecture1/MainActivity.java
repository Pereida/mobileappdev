package ee.ut.cs.cpereida.mad_2015_lecture1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create a new Layout to put the Text Views there
        LinearLayout mainLayout = new LinearLayout(this);
        // Set the orientation to Vertical
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        // Let us try with horizontal
//        mainLayout.setOrientation(LinearLayout.HORIZONTAL);
        // Create a log message
        Log.v("MainActivity.java", "Set orientation to horizontal");

        // Create Text view and set the text
        TextView t1 = new TextView(this);
        t1.setText(R.string.hello_world);

        TextView t2 = new TextView(this);
        t2.setText(R.string.dicenquesoy);

        TextView t3 = new TextView(this);
        t3.setText("Que soy un loco!");

        // Add the views to the layout
        mainLayout.addView(t1);
        mainLayout.addView(t2);
        mainLayout.addView(t3);

        // set the content to layout
        setContentView(mainLayout);
//        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
